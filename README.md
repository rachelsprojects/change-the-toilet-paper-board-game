# CHANGE the TOILET PAPER

Board game by Rachel Singh

For Rebekah Gall as a Christmas gift

## Included

All art assets (board pieces, manuel graphics) and the rules are included in this repository.

## Photos

![The game box](Pictures/final-box.jpg)

The game box

![The entire game](Pictures/full-set.jpg)

The full set

![Items](Pictures/item.jpg)

All the game items

![Characters](Pictures/characters.jpg)

The game characters

![Board and instructions](Pictures/board-and-instructions.jpg)

The board and the instructions
